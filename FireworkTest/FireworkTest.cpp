﻿
#include "stdafx.h"

#include "FireworkTest.h"

namespace Engine
{
	Engine::CLog Log;
	Engine::C3DEngine engine;
}

using namespace Engine;

#define MAX_LOADSTRING 100
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

// Глобальные переменные:
HINSTANCE hInst;
HWND hWnd;

bool CreateScene()
{
	V_TRUE(engine.AddFireWork_2D());

	return true;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		PAINTSTRUCT paintStruct;
		HDC hDC;

		case WM_PAINT:
		{
			hDC = BeginPaint(hWnd, &paintStruct);
			EndPaint(hWnd, &paintStruct);
		}

		break;

		case WM_SIZE:
		{
			if (wParam == SIZE_MINIMIZED)
				return 0;

			if (!engine.Resize())
				DestroyWindow(hWnd);

			return 0;
		}

		case WM_CLOSE:
		{
			DestroyWindow(hWnd);

			return 0;
		}

		case WM_DESTROY:
		{
			PostQuitMessage(0);

			return 0;
		}
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

bool Initialization()
{
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInst;
	wc.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_FIREWORKTEST));
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = szWindowClass;
	wc.hIconSm = LoadIcon(hInst, MAKEINTRESOURCE(IDI_FIREWORKTEST));

	if (!RegisterClassEx(&wc))
		return false;

	int screenX = GetSystemMetrics(SM_CXSCREEN);
	int screenY = GetSystemMetrics(SM_CYSCREEN);
	hWnd = CreateWindowEx(
		0,
		szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		(screenX - wndWidth) / 2,
		(screenY - wndHeight) / 2,
		wndWidth,
		wndHeight,
		nullptr,
		nullptr,
		hInst,
		nullptr);

	if (!hWnd)
		return false;

	V_TRUE(engine.Initialization(hWnd));
	V_TRUE(CreateScene());

	ShowWindow(hWnd, true);
	UpdateWindow(hWnd);

	return true;
}

int APIENTRY wWinMain(_In_ HINSTANCE		hInstance,
					  _In_opt_ HINSTANCE	hPrevInstance,
					  _In_ LPWSTR			lpCmdLine,
					  _In_ int				nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	hInst = hInstance;

    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_FIREWORKTEST, szWindowClass, MAX_LOADSTRING);

	if (!Initialization())
		return -1;

    // Цикл обработки сообщений:
    MSG msg = { 0 };
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
			if (!engine.Render())
				DestroyWindow(hWnd);
	}

    return (int)msg.wParam;
}