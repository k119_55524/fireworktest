#pragma once

#include "CLog.h"
#include "List.h"
#include "CBaseMaterial.h"
#include "CResourceTexture.h"

using namespace Templates;

namespace Engine
{
	class C3DEngine final
	{
	public:
		C3DEngine();
		~C3DEngine();

		bool Initialization(HWND hwnd);
		bool Render();
		bool Resize();

		bool AddFireWork_2D();

	private:
		HWND hWnd;
		bool f_Init = false;
		CLog Log;
		float m_allTime;
		float m_deltaTime;

		IDXGIFactory1*			g_pFactory			= nullptr;
		ID3D11Device*			g_p3dDevice			= nullptr;
		ID3D11DeviceContext*	g_pImmediateContext	= nullptr;
		IDXGISwapChain* 		g_pSwapChain		= nullptr;
		ID3D11RenderTargetView*	g_pRenderTargetView = nullptr;

		CList<CBaseMaterial*> materials;

		//std::unique_ptr<int> i;

		bool InitializationResource();
		void UpdateTime();
		void UpdatePhysics();

		CResourceTexture texture;
	};
}