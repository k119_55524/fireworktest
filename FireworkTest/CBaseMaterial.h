#pragma once

#include "CLog.h"
#include "CBaseShader.h"

namespace Engine
{
	class CBaseMaterial
	{
	public:
		CBaseMaterial();
		virtual ~CBaseMaterial();

		inline bool Initialization()
		{
			if (_Initialization())
			{
				f_Init = true;
				return true;
			}

			return false;
		}

		// ������������� ������� ��������(������)
		inline bool SetMaterial()
		{
			if (!f_Init)
				return false;

			return shader->SetShader();
		}
		// ������������� �������������� ��������� ���������
		virtual void SetMaterialParameter() = 0;

	protected:
		CBaseShader* shader;

		virtual bool _Initialization() = 0;
		//virtual void Release() = 0;

	private:
		bool f_Init;
		//void _Release();
	};
}
