#pragma once

#include "CBaseShader.h"

namespace Engine
{
	class CShaderFireWork_2D :
		public CBaseShader
	{
	public:
		CShaderFireWork_2D();
		~CShaderFireWork_2D();

	protected:
		virtual bool _Initialization() override;
	};
}