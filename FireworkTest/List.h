#pragma once

#include "stdafx.h"

#include "CLog.h"

using namespace Engine;

namespace Templates
{
	template <class DataT> class CListObj
	{
	public:
		CListObj(void);
		inline void GetInfo(DataT &c) { c = info; };
		inline CListObj<DataT>* GetNext(void) { return next; };
		inline CListObj<DataT>* GetPrior(void) { return prior; };

		CListObj<DataT>*			next;
		CListObj<DataT>*			prior;
		DataT						info;
	};

	template <class DataT> CListObj<DataT>::CListObj(void) :
		info(nullptr),
		next(nullptr),
		prior(nullptr)
	{
	};

	template <class DataT> class CList : public CListObj <DataT>
	{

	public:
		CList();
		~CList();
		void ReleaseLP();

		bool Add(DataT c);
		void ClearAll();
		inline UINT GetSize() { return size; };
		bool FindAndCutObject(DataT val);
		bool CutObject(CListObj<DataT> *p);
		inline bool FindStart()
		{
			if (size == 0)
				return false;

			findCur = start;

			return true;
		};
		inline bool FindNext()
		{
			if (findCur->next != nullptr)
			{
				findCur = findCur->next;
				return true;
			}
			return false;
		};
		inline DataT GetFindObject(void) { return findCur->info; };
		inline void GetFindObject(DataT &fData) { fData = findCur->info; };

	protected:
		CListObj<DataT>	*start, *end, *cur, *findCur;
		UINT size;
	};

	template <class DataT> CList<DataT>::CList(void) :
		start(nullptr),
		end(nullptr),
		cur(nullptr),
		findCur(nullptr),
		size(0)
	{
	}

	template <class DataT> CList<DataT>::~CList(void)
	{
		CListObj<DataT>	*p, *p1;

		p = start;

		while (p)
		{
			p1 = p->next;
			delete(p);
			p = p1;
		}

		start = end = cur = nullptr;
		size = 0;
	}

	template <class DataT> void CList<DataT>::ReleaseLP(void)
	{
		CListObj<DataT>	*p, *p1;

		p = start;

		while (p)
		{
			delete p->info;

			p1 = p->next;

			delete(p);
			p = p1;
		}

		start = end = cur = nullptr;
		size = 0;
	}

	template <class DataT> bool CList<DataT>::Add(DataT c)
	{
		CListObj<DataT>	*p;

		p = new CListObj<DataT>;
		if (!p)
		{
			Engine::Log.OutLogFile_Error(L"Memory allocation failure.", __FILE__, __FUNCTION__, __LINE__);

			return false;
		}

		p->info = c;

		if (size == 0)
			end = start = cur = p;
		else
		{
			p->prior = end;
			end->next = p;
			end = p;
		}

		size++;

		return false;
	}

	template <class DataT> void CList<DataT>::ClearAll(void)
	{
		CListObj<DataT>	*p, *p1;

		p = start;

		while (p)
		{
			p1 = p->next;
			delete(p);
			p = p1;
		}

		start = end = cur = nullptr;
		size = 0;
	}

	template <class DataT> bool CList<DataT>::FindAndCutObject(DataT val)
	{
		CListObj<DataT>	*p;

		p = start;

		while (p)
		{
			if (p->info == val)
			{
				if (size == 1)
				{
					delete p;
					start = end = cur = findCur = nullptr;
					size = 0;

					return true;
				}
				if (p->next != nullptr)
					p->next->prior = p->prior;
				else
				{
					p->prior->next = nullptr;
					end = p->prior;
				}
				if (p->prior != nullptr)
					p->prior->next = p->next;
				else
				{
					p->next->prior = nullptr;
					start = p->next;
				}

				delete p;
				size--;

				return true;
			}
			p = p->next;
		}

		return false;
	}

	template <class DataT> bool CList<DataT>::CutObject(CListObj<DataT> *p)
	{
		if (size == 1)
		{
			start = end = cur = findCur = nullptr;
			size = 0;

			return true;
		}
		if (p->next != nullptr)
			p->next->prior = p->prior;
		else
		{
			p->prior->next = nullptr;
			end = p->prior;
		}
		if (p->prior != nullptr)
			p->prior->next = p->next;
		else
		{
			p->next->prior = nullptr;
			start = p->next;
		}

		size--;

		return true;
	}
}