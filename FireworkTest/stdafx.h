﻿
#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Исключите редко используемые компоненты из заголовков Windows
// Файлы заголовков Windows
#include <windows.h>

#include <string>
#include <sstream>

//#include <memory>
//#include <tchar.h>
//#include <winnt.h>
//#include <stdlib.h>
//#include <malloc.h>

#include <d3d11_4.h>
#include <DirectXMath.h>

//#include <d3d11.h>
//#include <d3dukmdt.h>
//#include <d3dcompiler.h>
//#include <d3d11x.h>
//#include <wrl.h>
//#include <d2d1_3.h>
//#include <d2d1effects_2.h>
//#include <dwrite_3.h>
//#include <wincodec.h>
//#include <DirectXColors.h>
//#include <agile.h>
//#include <concrt.h>

using namespace std;
using namespace DirectX;

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
//#pragma comment(lib, "d3dcompiler.lib")
//#pragma comment(lib, "winmm.lib")
//d2d1.lib
//windowscodecs.lib
//dwrite.lib

// Формат текстуры буффера рендринга
#define FORMAT_RENDER_BUFFER DXGI_FORMAT_R8G8B8A8_UNORM

#define V_S_OK(x) \
		{ \
			HRESULT res = (x); \
			if (res != S_OK) \
			{ \
				Log.OutLogFile_Error(res, __FILE__, __FUNCTION__, __LINE__); \
				\
				return false; \
			} \
		 };

#define V_TRUE(x) \
		{ \
			if (x != true) \
			{ \
				Log.OutLogFile_Error(x, __FILE__, __FUNCTION__, __LINE__); \
				\
				return x; \
				} \
		};

template<typename T>
inline void SafeRelease(T& ptr)
{
	if (ptr != nullptr)
	{
		ptr->Release();
		ptr = nullptr;
	}
}