
#pragma once

#include "DDSTextureLoader.h"

namespace Engine
{
	extern class CLog Log;

	class CResourceTexture
	{
	public:
		CResourceTexture();
		~CResourceTexture();

		bool CreateTextureOfFile(ID3D11Device* device, std::wstring fileName);
		inline ID3D11ShaderResourceView* GetSRV(void) { return g_pTextureSRV; };

		inline UINT GetWidth(void) { return m_Width; };
		inline UINT GetHeight(void) { return m_Height; };

	private:
		std::wstring	m_FileName;
		UINT			m_Width		= 0;
		UINT			m_Height	= 0;
		UINT			m_MipLevels	= 0;

		ID3D11Resource*				g_pTextureRes = nullptr;
		ID3D11ShaderResourceView*	g_pTextureSRV = nullptr;

		void Release();
	};
}