
#include "stdafx.h"

//--------------------------------------------------------------------------------------
// File: DDSTextureLoader.cpp
//
// Functions for loading a DDS texture and creating a Direct3D 11 runtime resource for it
//
// Note these functions are useful as a light-weight runtime loader for DDS files. For
// a full-featured DDS file reader, writer, and texture processing pipeline see
// the 'Texconv' sample and the 'DirectXTex' library.
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//
// http://go.microsoft.com/fwlink/?LinkId=248926
// http://go.microsoft.com/fwlink/?LinkId=248929
//--------------------------------------------------------------------------------------

#include "DDSTextureLoader.h" 

using namespace MainDirectX;

//--------------------------------------------------------------------------------------
_Use_decl_annotations_
HRESULT MainDirectX::CreateDDSTextureFromMemory( ID3D11Device* d3dDevice,
                                             const uint8_t* ddsData,
                                             size_t ddsDataSize,
                                             ID3D11Resource** texture,
                                             ID3D11ShaderResourceView** textureView,
                                             size_t maxsize,
                                             DDS_ALPHA_MODE* alphaMode )
{
    return CreateDDSTextureFromMemoryEx( d3dDevice, nullptr, ddsData, ddsDataSize, maxsize,
                                         D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, false,
                                         texture, textureView, alphaMode );
}

_Use_decl_annotations_
HRESULT MainDirectX::CreateDDSTextureFromMemory( ID3D11Device* d3dDevice,
                                             ID3D11DeviceContext* d3dContext,
                                             const uint8_t* ddsData,
                                             size_t ddsDataSize,
                                             ID3D11Resource** texture,
                                             ID3D11ShaderResourceView** textureView,
                                             size_t maxsize,
                                             DDS_ALPHA_MODE* alphaMode )
{
    return CreateDDSTextureFromMemoryEx( d3dDevice, d3dContext, ddsData, ddsDataSize, maxsize,
                                         D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, false,
                                         texture, textureView, alphaMode );
}

_Use_decl_annotations_
HRESULT MainDirectX::CreateDDSTextureFromMemoryEx( ID3D11Device* d3dDevice,
                                               const uint8_t* ddsData,
                                               size_t ddsDataSize,
                                               size_t maxsize,
                                               D3D11_USAGE usage,
                                               unsigned int bindFlags,
                                               unsigned int cpuAccessFlags,
                                               unsigned int miscFlags,
                                               bool forceSRGB,
                                               ID3D11Resource** texture,
                                               ID3D11ShaderResourceView** textureView,
                                               DDS_ALPHA_MODE* alphaMode )
{
    return CreateDDSTextureFromMemoryEx( d3dDevice, nullptr, ddsData, ddsDataSize, maxsize,
                                         usage, bindFlags, cpuAccessFlags, miscFlags, forceSRGB,
                                         texture, textureView, alphaMode );
}

_Use_decl_annotations_
HRESULT MainDirectX::CreateDDSTextureFromMemoryEx( ID3D11Device* d3dDevice,
                                               ID3D11DeviceContext* d3dContext,
                                               const uint8_t* ddsData,
                                               size_t ddsDataSize,
                                               size_t maxsize,
                                               D3D11_USAGE usage,
                                               unsigned int bindFlags,
                                               unsigned int cpuAccessFlags,
                                               unsigned int miscFlags,
                                               bool forceSRGB,
                                               ID3D11Resource** texture,
                                               ID3D11ShaderResourceView** textureView,
                                               DDS_ALPHA_MODE* alphaMode )
{
    if ( texture )
    {
        *texture = nullptr;
    }
    if ( textureView )
    {
        *textureView = nullptr;
    }
    if ( alphaMode )
    {
        *alphaMode = DDS_ALPHA_MODE_UNKNOWN;
    }

    if (!d3dDevice || !ddsData || (!texture && !textureView))
    {
        return E_INVALIDARG;
    }

    // Validate DDS file in memory
    if (ddsDataSize < (sizeof(uint32_t) + sizeof(DDS_HEADER)))
    {
        return E_FAIL;
    }

    uint32_t dwMagicNumber = *( const uint32_t* )( ddsData );
    if (dwMagicNumber != DDS_MAGIC)
    {
        return E_FAIL;
    }

    auto header = reinterpret_cast<const DDS_HEADER*>( ddsData + sizeof( uint32_t ) );

    // Verify header to validate DDS file
    if (header->size != sizeof(DDS_HEADER) ||
        header->ddspf.size != sizeof(DDS_PIXELFORMAT))
    {
        return E_FAIL;
    }

    // Check for DX10 extension
    bool bDXT10Header = false;
    if ((header->ddspf.flags & DDS_FOURCC) &&
        (MAKEFOURCC( 'D', 'X', '1', '0' ) == header->ddspf.fourCC) )
    {
        // Must be long enough for both headers and magic value
        if (ddsDataSize < (sizeof(DDS_HEADER) + sizeof(uint32_t) + sizeof(DDS_HEADER_DXT10)))
        {
            return E_FAIL;
        }

        bDXT10Header = true;
    }

    ptrdiff_t offset = sizeof( uint32_t )
                       + sizeof( DDS_HEADER )
                       + (bDXT10Header ? sizeof( DDS_HEADER_DXT10 ) : 0);

    HRESULT hr = CreateTextureFromDDS( d3dDevice, d3dContext, header,
                                       ddsData + offset, ddsDataSize - offset, maxsize,
                                       usage, bindFlags, cpuAccessFlags, miscFlags, forceSRGB,
                                       texture, textureView );
    if ( SUCCEEDED(hr) )
    {
        if (texture != 0 && *texture != 0)
        {
            SetDebugObjectName(*texture, "DDSTextureLoader");
        }

        if (textureView != 0 && *textureView != 0)
        {
            SetDebugObjectName(*textureView, "DDSTextureLoader");
        }

        if ( alphaMode )
            *alphaMode = GetAlphaMode( header );
    }

    return hr;
}

//--------------------------------------------------------------------------------------
_Use_decl_annotations_
HRESULT MainDirectX::CreateDDSTextureFromFile( ID3D11Device* d3dDevice,
                                           const wchar_t* fileName,
                                           ID3D11Resource** texture,
                                           ID3D11ShaderResourceView** textureView,
                                           size_t maxsize,
                                           DDS_ALPHA_MODE* alphaMode )
{
    return CreateDDSTextureFromFileEx( d3dDevice, nullptr, fileName, maxsize,
                                       D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, false,
                                       texture, textureView, alphaMode );
}

_Use_decl_annotations_
HRESULT MainDirectX::CreateDDSTextureFromFile( ID3D11Device* d3dDevice,
                                           ID3D11DeviceContext* d3dContext,
                                           const wchar_t* fileName,
                                           ID3D11Resource** texture,
                                           ID3D11ShaderResourceView** textureView,
                                           size_t maxsize,
                                           DDS_ALPHA_MODE* alphaMode )
{
    return CreateDDSTextureFromFileEx( d3dDevice, d3dContext, fileName, maxsize,
                                       D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, false,
                                       texture, textureView, alphaMode );
}

_Use_decl_annotations_
HRESULT MainDirectX::CreateDDSTextureFromFileEx( ID3D11Device* d3dDevice,
                                             const wchar_t* fileName,
                                             size_t maxsize,
                                             D3D11_USAGE usage,
                                             unsigned int bindFlags,
                                             unsigned int cpuAccessFlags,
                                             unsigned int miscFlags,
                                             bool forceSRGB,
                                             ID3D11Resource** texture,
                                             ID3D11ShaderResourceView** textureView,
                                             DDS_ALPHA_MODE* alphaMode )
{
    return CreateDDSTextureFromFileEx( d3dDevice, nullptr, fileName, maxsize,
                                       usage, bindFlags, cpuAccessFlags, miscFlags, forceSRGB,
                                       texture, textureView, alphaMode );
}

_Use_decl_annotations_
HRESULT MainDirectX::CreateDDSTextureFromFileEx( ID3D11Device* d3dDevice,
                                             ID3D11DeviceContext* d3dContext,
                                             const wchar_t* fileName,
                                             size_t maxsize,
                                             D3D11_USAGE usage,
                                             unsigned int bindFlags,
                                             unsigned int cpuAccessFlags,
                                             unsigned int miscFlags,
                                             bool forceSRGB,
                                             ID3D11Resource** texture,
                                             ID3D11ShaderResourceView** textureView,
                                             DDS_ALPHA_MODE* alphaMode )
{
    if ( texture )
    {
        *texture = nullptr;
    }
    if ( textureView )
    {
        *textureView = nullptr;
    }
    if ( alphaMode )
    {
        *alphaMode = DDS_ALPHA_MODE_UNKNOWN;
    }

    if (!d3dDevice || !fileName || (!texture && !textureView))
    {
        return E_INVALIDARG;
    }

    DDS_HEADER* header = nullptr;
    uint8_t* bitData = nullptr;
    size_t bitSize = 0;

    std::unique_ptr<uint8_t[]> ddsData;
    HRESULT hr = LoadTextureDataFromFile( fileName,
                                          ddsData,
                                          &header,
                                          &bitData,
                                          &bitSize
                                        );
    if (FAILED(hr))
    {
        return hr;
    }

    hr = CreateTextureFromDDS( d3dDevice, d3dContext, header,
                               bitData, bitSize, maxsize,
                               usage, bindFlags, cpuAccessFlags, miscFlags, forceSRGB,
                               texture, textureView );

    if ( SUCCEEDED(hr) )
    {
#if !defined(NO_D3D11_DEBUG_NAME) && ( defined(_DEBUG) || defined(PROFILE) )
        if (texture != 0 || textureView != 0)
        {
            CHAR strFileA[MAX_PATH];
            int result = WideCharToMultiByte( CP_ACP,
                                              WC_NO_BEST_FIT_CHARS,
                                              fileName,
                                              -1,
                                              strFileA,
                                              MAX_PATH,
                                              nullptr,
                                              FALSE
                               );
            if ( result > 0 )
            {
                const CHAR* pstrName = strrchr( strFileA, '\\' );
                if (!pstrName)
                {
                    pstrName = strFileA;
                }
                else
                {
                    pstrName++;
                }

                if (texture != 0 && *texture != 0)
                {
                    (*texture)->SetPrivateData( WKPDID_D3DDebugObjectName,
                                                static_cast<UINT>( strnlen_s(pstrName, MAX_PATH) ),
                                                pstrName
                                              );
                }

                if (textureView != 0 && *textureView != 0 )
                {
                    (*textureView)->SetPrivateData( WKPDID_D3DDebugObjectName,
                                                    static_cast<UINT>( strnlen_s(pstrName, MAX_PATH) ),
                                                    pstrName
                                                  );
                }
            }
        }
#endif

        if ( alphaMode )
            *alphaMode = GetAlphaMode( header );
    }

    return hr;
}
