
#include "stdafx.h"

#include "C3DEngine.h"

using namespace Engine;

C3DEngine::C3DEngine()
{
}

C3DEngine::~C3DEngine()
{
	materials.ReleaseLP();

	SafeRelease(g_pRenderTargetView);
	SafeRelease(g_pSwapChain);
	SafeRelease(g_pImmediateContext);
	SafeRelease(g_p3dDevice);
	SafeRelease(g_pFactory);
}

bool C3DEngine::Initialization(HWND hwnd)
{
	hWnd = hwnd;

	V_S_OK(CreateDXGIFactory1(__uuidof(IDXGIFactory), (void**)(&g_pFactory)));

	UINT creationFlags = 0;
	#if defined( DEBUG ) || defined( _DEBUG )
		creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
	#endif;

	D3D_FEATURE_LEVEL m_d3dFeatureLevel;
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		//D3D_FEATURE_LEVEL_10_1,
		//D3D_FEATURE_LEVEL_10_0,
		//D3D_FEATURE_LEVEL_9_3,
		//D3D_FEATURE_LEVEL_9_2,
		//D3D_FEATURE_LEVEL_9_1 
	};

	V_S_OK(D3D11CreateDevice(
		nullptr,					// �������� nullptr ��� ������������� �������� �� ���������.
		D3D_DRIVER_TYPE_HARDWARE,	// �������� ���������� � ������� �������� ������������ ������������.
		0,							// ������ ��������� 0, ���� ������� �� ����� D3D_DRIVER_TYPE_SOFTWARE.
		creationFlags,				// ��������� ������ ������� � ������������� � Direct2D.
		featureLevels,				// ������ �������������� �������, ������� ����� �������������� ���� �����������.
		ARRAYSIZE(featureLevels),	// ������ ���������������� ������.
		D3D11_SDK_VERSION,			// ������ ������������� ������ D3D11_SDK_VERSION ��� ���������� �������� Windows.
		&g_p3dDevice,				// ���������� ��������� ���������� Direct3D.
		&m_d3dFeatureLevel,			// ���������� ������� ������������ ���������� ����������.
		&g_pImmediateContext));		// ���������� ���������� �������� ����������.

	RECT rc;
	GetClientRect(hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	DXGI_SWAP_CHAIN_DESC scd;
	ZeroMemory(&scd, sizeof(scd));
	scd.BufferDesc.Width = width;
	scd.BufferDesc.Height = height;
	scd.BufferDesc.Format = FORMAT_RENDER_BUFFER;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 1;
	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.BufferCount = 1;
	scd.OutputWindow = hWnd;
	scd.Windowed = true;
	V_S_OK(g_pFactory->CreateSwapChain(g_p3dDevice, &scd, &g_pSwapChain));

	V_TRUE(InitializationResource());

	f_Init = true;
	
	return true;
}

bool C3DEngine::InitializationResource()
{
	V_TRUE(texture.CreateTextureOfFile(g_p3dDevice, L"Textures/test.dds"));

	return true;
}

bool C3DEngine::Render()
{
	if (!f_Init)
		return false;

	UpdateTime();

	UpdatePhysics(); // �� ����  :)

	g_pImmediateContext->OMSetRenderTargets(1, &g_pRenderTargetView, nullptr);

	const float clearColor[4] = { 1.0f / 256.0f * 66.0f, 1.0f / 256.0f * 163.0f, 1.0f / 256.0f * 237.0f, 1.0f };
	g_pImmediateContext->ClearRenderTargetView(g_pRenderTargetView, clearColor);

	g_pSwapChain->Present(0, 0);

	return true;
}

void C3DEngine::UpdatePhysics()
{

}

void C3DEngine::UpdateTime()
{
	static DWORD m_TimerStart = GetTickCount();
	static DWORD m_OtdFrameTime = m_TimerStart;

	DWORD TimerCur = GetTickCount();

	m_allTime = (float)((double)TimerCur - (double)m_TimerStart) / 1000.0f;
	m_deltaTime = (float)((double)TimerCur - (double)m_OtdFrameTime) / 1000.0f;

	m_OtdFrameTime = TimerCur;
}

bool C3DEngine::Resize()
{
	if (!f_Init)
		return false;

	UINT width, height;
	BOOL f_FullScreen;

	// ��������� ����� ����� ���� ������ ����������
	g_pSwapChain->GetFullscreenState(&f_FullScreen, nullptr);
	if (!f_FullScreen)
	{
		RECT rc;
		GetClientRect(hWnd, &rc);
		width = rc.right - rc.left;
		height = rc.bottom - rc.top;
	}
	else
	{
		width = GetSystemMetrics(SM_CXSCREEN);
		height = GetSystemMetrics(SM_CYSCREEN);
	};

	V_S_OK(g_pSwapChain->ResizeBuffers(
		2,							// ����� ���������� ������� � ������� ������������
		0,							// ����� ������
		0,							// ����� ������
		FORMAT_RENDER_BUFFER,		// ����� ������ ���������� ������
		0));						// �������������� �����

		// ���������� ���������� �� ��������� �������� � ������� ������������
	ID3D11Texture2D* g_pBackBuffer = nullptr;
	V_S_OK(g_pSwapChain->GetBuffer(
		0,							// ������ ���������� ������� � �������� ����� �������� ������
		__uuidof(ID3D11Texture2D),	// ������������� ���������� ������� ������������ ��� ���������� ���������
		(LPVOID*)&g_pBackBuffer));	// ����� ��������� ����������� ���������� �������

	// ������ ������������� ������ ��� ���������� ������� ������������
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	g_pBackBuffer->GetDesc(&descDepth);

	D3D11_RENDER_TARGET_VIEW_DESC rt;
	ZeroMemory(&rt, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	rt.Format = descDepth.Format;
	rt.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rt.Texture2D.MipSlice = 0;

	SafeRelease(g_pRenderTargetView);
	HRESULT res = g_p3dDevice->CreateRenderTargetView(
		g_pBackBuffer,			// ��������� �� ������ ���������� ������
		&rt,					// ��������� �� ��������� ���������� ��������� ������������
		&g_pRenderTargetView);	// ����� ��������� �� ��������� ������������� ������

	SafeRelease(g_pBackBuffer);

	D3D11_VIEWPORT m_ViewPort;
	m_ViewPort.Width = (float)width;
	m_ViewPort.Height = (float)height;
	m_ViewPort.MinDepth = 0.0f;
	m_ViewPort.MaxDepth = 1.0f;
	m_ViewPort.TopLeftX = 0;
	m_ViewPort.TopLeftY = 0;
	g_pImmediateContext->RSSetViewports(1, &m_ViewPort);

	if (res != S_OK)
		return false;

	return true;
}

bool C3DEngine::AddFireWork_2D()
{
	if (!f_Init)
		return false;

	return true;
}