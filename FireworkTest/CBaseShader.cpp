
#include "stdafx.h"

#include "CBaseShader.h"

using namespace Engine;

CBaseShader::CBaseShader() :
	f_Init(false)
{
}
 
CBaseShader::~CBaseShader()
{
}

bool CBaseShader::SetShader()
{
	if (!f_Init)
		return false;

	return true;
}