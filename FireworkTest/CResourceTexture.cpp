
#include "stdafx.h"

#include "CLog.h"
#include "CResourceTexture.h"

using namespace Engine;

CResourceTexture::CResourceTexture()
{
}

CResourceTexture::~CResourceTexture()
{
	Release();
}

void CResourceTexture::Release()
{
	SafeRelease(g_pTextureSRV);
	SafeRelease(g_pTextureRes);
}

bool CResourceTexture::CreateTextureOfFile(ID3D11Device* device, std::wstring fileName)
{
	Release();

	V_S_OK(MainDirectX::CreateDDSTextureFromFile(device, fileName.c_str(), &g_pTextureRes, &g_pTextureSRV));

	ID3D11Texture2D* texture2d = nullptr;
	V_S_OK(g_pTextureRes->QueryInterface(&texture2d));

	D3D11_TEXTURE2D_DESC desc;
	texture2d->GetDesc(&desc);
	m_Width = desc.Width;
	m_Height = desc.Height;
	m_MipLevels = desc.MipLevels;

	SafeRelease(texture2d);

	return true;
}