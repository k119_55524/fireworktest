#pragma once

#include "CBaseMaterial.h"

namespace Engine
{
	class CMatFireWork : CBaseMaterial
	{
	public:
		CMatFireWork();
		~CMatFireWork();

		virtual void SetMaterialParameter() override;

	protected:
		virtual bool _Initialization() override;
		//virtual void Release() override;
	};
}