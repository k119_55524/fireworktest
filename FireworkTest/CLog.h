#pragma once

namespace Engine
{
	class CLog final
	{
	public:
		CLog();
		~CLog();

		inline void OutLogFile_Error(HRESULT res, const char* file, const char* func, UINT line)
		{
			#if defined( DEBUG ) || defined( _DEBUG )
				std::wstringstream st;
				st << "Result != S_OK" << "\n";
				st << "File: " << file << "\n";
				st << "Line: " << line << "\n";

				MessageBox(NULL, st.str().c_str(), L"Error!!!", MB_OK | MB_ICONSTOP | MB_TASKMODAL);
			#endif

			// Output in file....
		}

		inline void OutLogFile_Error(bool res, const char* file, const char* func, UINT line)
		{
			#if defined( DEBUG ) || defined( _DEBUG )
				std::wstringstream st;

				if (res)
					st << "Result = true" << "\n";
				else
					st << "Result = false" << "\n";

				st << "File: " << file << "\n";
				st << "Line: " << line << "\n";

				MessageBox(NULL, st.str().c_str(), L"Error!!!", MB_OK | MB_ICONSTOP | MB_TASKMODAL);
			#endif

			// Output in file....
		}

		inline void OutLogFile_Error(std::wstring errText, const char* file, const char* func, UINT line)
		{
			#if defined( DEBUG ) || defined( _DEBUG )
				std::wstringstream st;

				st << "Error: " << errText << "\n";
				st << "File: " << file << "\n";
				st << "Line: " << line << "\n";

				MessageBox(NULL, st.str().c_str(), L"Error!!!", MB_OK | MB_ICONSTOP | MB_TASKMODAL);
			#endif

			// Output in file....
		}
	};
}