#pragma once

namespace Engine
{
	class CBaseShader
	{
	public:
		CBaseShader();
		virtual ~CBaseShader();

		inline bool Initialization()
		{
			if (_Initialization())
			{
				f_Init = true;
				return true;
			}

			return false;
		}

		bool SetShader();

	protected:
		virtual bool _Initialization() = 0;

	private:
		bool f_Init;
	};
}